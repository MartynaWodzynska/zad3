---
author: Martyna Wodzyńska
title: Pretérito - czasy przeszłe
subtitle: 
date: 
theme: Warsaw
output: beamer_presentation
header-includes:  
    \usepackage{xcolor}
    \usepackage{listings}
---
## Slajd 1 

Hiszpańskie czasy przeszłe:

* **Pretérito Perfecto** - czas przeszły dokonany złożony
* **Pretérito Indefinido** - czas przeszły dokonany
* **Pretérito Imperfecto** - czas przeszły niedokonany
* **Pretérito Pluscuamperfecto** - czas zaprzeszły
  
## Slajd 2 Pretérito Perfecto


1. FORMA
   
**HABER + PARTICIPIO** (habl~~AR~~ - habl**ADO**, com~~ER~~ - com**IDO**, viv~~IR~~ - viv**IDO** )

|          | HABER      | + PARTICIPIO  |
| -------- |:----------:| :------------:|
| YO       | HE         | HABL**ADO**   |
| TÚ       | HAS        |   BEB**IDO**  |
| ÉL, ELLA | HA         |  VIV**IDO**   |
| NOSOTROS | HEMOS      | EST**ADO**    |
| VOSTOTROS| HABÉIS     |   COM**IDO**  |
| ELLOS, ELLAS| HAN     |  SAL**IDO**   |

## Slajd 3 Pretérito Perfecto

2. Usos
   
![](2.png)

- Acciones pasados en un tiempo relacionado con el presente:
  <p>*Esta mañana he desayunado un café con leche y dos tostadas.*</p>
  <p>*Este año no han viajando mucho.*</p>

- Para hablar de experiencias-acciones pasada (realizada o no) sin especificar el momento:
  <p>*Alguna vez has ido a Brasil.*</p>
  <p>*Si, ya he estado en Brasil.*</p>
   <p>*No, todavía no he estado en Brasil.*</p>

## Slajd 4 Pretérito Indefínido

1. FORMA

|          | -AR           | -ER           | -IR           | 
| -------- |:--------------| :------------:|:----------:|
|          |HABLAR          |BEBER        |VIVIR
| YO       |HABL**É**      |BEB**Í**     |VIV**Í**
| TÚ       |HABL**ASTE**   |BEB**ISTE**  |VIV**ISTE** 
| ÉL, ELLA |HABL**Ó**      |BEB**IÓ**    |VIVI**Ó**
| NOSOTROS |HABL**AMOS**   |BEB**IMOS**  |VIV**IMOS** 
| VOSTOTROS|HABL**ASTEIS** |BEB**ISTEIS**|VIV**ISTEIS**
| ELLOS, ELLAS|HABL**ARON**|BEB**IERON** |VIV**IERON** 

## Slajd 5 Pretérito Indefínido

2. Usos
   
![](3.png)

- Acciones concretas y terminadas en el pasado:</br>
  <p>*Ayer por la noche **fui** al cine.*</p>
- Sucesíon de acciones en el pasado:
  <p>*Luis se **levantó** a las 7, **desayunó** y se **fue** al gimnasio.*</p>
- Acctiones interrumpidas por otra acción
  <p>*Estaba durmiendo cuando **sonó** el móvil*</p>

## Slajd 6 Pretérito Imperfecto

1. FORMA
   
|          | -AR           |   -ER       | -IR        |
| -------- |:-------------:|:-----------:|:-----------:
|          |HABLAR         |BEBER        |VIVIR
| YO       |HABL**ABA**    |BEB**ÍA**    |VIV**ÍA**
| TÚ       |HABL**ABAS**   |BEB**ÍAS**   |VIV**ÍAS** 
| ÉL, ELLA |HABL**ABA**    |BEB**ÍA**    |VIVI**ÍA**
| NOSOTROS |HABL**ÁBAMOS** |BEB**ÍAMOS** |VIV**ÍAMOS** 
| VOSTOTROS|HABL**ABAIS**  |BEB**ÍAIS**  |VIV**ÍAIS**
| ELLOS, ELLAS|HABL**ABAN**|BEB**ÍAN**   |VIV**ÍAN**


## Slajd 7 Pretérito Imperfecto

2. Usos

<p></p>  

::: {.columns}
:::: {.column width=0.25}
<p>**Edad:**</p>
<p>**Condición fisica:**</p>
<p>**Apariencia:**</p>
<p>**Clima:**</p>
<p>**Horario:**</p>
<p>**Actitud y deseos:**</p>
<p>**Localidad:**</p>
<p>**Emoción:**</p>
::::
:::: {.column width=0.75}
<p>*Cuando **tenía** 8 años, me gustaba ir al zoológico*</p>
<p>*Después de mis clases, **estaba** cansado y **tenía** sueño*</p>
<p>*Mi mejor amiga **era** morena y **tenía** ojos verdes*</p>
<p>***Hacía** frío en el invierno y **llovía** mucho*</p>
<p>***Eran** las 8 de la mañaba cuando **iba** a la escuela*</p>
<p>***Quería** ser médico y mi hermano **deseaba** ser bombero*</p>
<p>*Mi casa **estaba** cerca de la playa*</p>
<p>*Yo **estaba** muy triste y **lloraba** cuando no podía salir a jugar con mis amigos*</p>
::::
:::

## Slajd 8 Pretérito Pluscuamperfecto

1. FORMA

**HABER + PARTICIPIO** 

|          | HABER      | + PARTICIPIO  |
| -------- |:----------:| :------------:|
| YO       | HABÍA      | HABL**ADO**   |
| TÚ       | HABÍAS     |   BEB**IDO**  |
| ÉL, ELLA | HABÍA      |  VIV**IDO**   |
| NOSOTROS | HABÍAMOS   | EST**ADO**    |
| VOSTOTROS| HABÍAIS    |   COM**IDO**  |
| ELLOS, ELLAS| HABÍAN  |  SAL**IDO**   |

## Slajd 9 Pretérito Pluscuamperfecto

2. USOS

![](4.PNG)

- Para expresar que una acción es anterior a otra en el pasado:
  <p>*Cuando llegué a la tienda ya **había cerrado**.*</p>

- Expresar que es la primera vez que realizamos una acción:
<p>*Nunca **había visitado** China. (Estoy en China ahora.)*</p>

- Expresar que una acción es inmediatamente posterior a otra en el pasado:  
<p>*Se conocieron en abril y en noviembre se **habían casado**. (Se casaron muy pronto.)*</p>

## Slajd 10 Ejemplos

### Perfecto o Indefinido
Hoy se ha levantado muy temprano.
Ayer se levantó muy temprano.

### Imperfecto
Cuando era niño se levantaba muy temprano.

### Pluscuamperfecto
Ella ya había levantado antes de el despertador sonó.